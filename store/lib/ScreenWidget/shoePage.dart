import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:store/DetailScreen/ItemDetail.dart';
import 'package:store/HomeWidget.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/controller/converjson.dart';
import 'package:store/models/ItemStore.dart';
import 'package:store/models/textStyle.dart';
import 'package:store/viewAll/moreShoes.dart';

class ShoesPage extends StatefulWidget {
  ShoesPage({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

void onTap(BuildContext context) {
  Navigator.of(context).pushNamed(ItemDetail.routeName, arguments: HotDeal());
}

class _HomeScreenState extends State<ShoesPage> {
  //bool loading = false;
  List<HotDeal> lstHotdeal = <HotDeal>[];
  List<HotDeal> lstShoes = <HotDeal>[];
  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await getHotdeal();
  }

  getHotdeal() async {
    await ServiceHotdeal.convertAssets().then((data) {
      setState(() {
        //loading = false;
        lstHotdeal = data;
        lstShoes =
            lstHotdeal.where((element) => element.category == 1).toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SingleChildScrollView(
            child: Column(children: <Widget>[
      SizedBox(
        height: 20,
      ),
      HomeWidget(context).buildSection("Shoes", () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => MoreShoes()));
      }),
      SizedBox(
        height: 20,
      ),
      Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Container(
          //height: 180,
          // decoration: BoxDecoration(color: Colors.blue),
          child: Stack(
            children: <Widget>[
              Container(
                height: 180,
                //color: Colors.pinkAccent,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: lstShoes.length,
                    //itemCount: 3,
                    itemBuilder: (_, i) => ItemStore(
                          item: lstShoes[i],
                        )),
              ),
            ],
          ),
        ),
      ),
    ])));
  }
}
