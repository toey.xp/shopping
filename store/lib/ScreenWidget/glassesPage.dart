import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:store/DetailScreen/ItemDetail.dart';
import 'package:store/HomeWidget.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/controller/converjson.dart';
import 'package:store/models/ItemStore.dart';
import 'package:store/models/textStyle.dart';
import 'package:store/viewAll/moreGlasses.dart';

class GlassesPage extends StatefulWidget {
  GlassesPage({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

void onTap(BuildContext context) {
  Navigator.of(context).pushNamed(ItemDetail.routeName, arguments: HotDeal());
}

class _HomeScreenState extends State<GlassesPage> {
  //bool loading = false;
  List<HotDeal> lstHotdeal = <HotDeal>[];
  List<HotDeal> lstGlasses = <HotDeal>[];
  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await getHotdeal();
  }

  getHotdeal() async {
    await ServiceHotdeal.convertAssets().then((data) {
      setState(() {
        //loading = false;
        lstHotdeal = data;
        lstGlasses =
            lstHotdeal.where((element) => element.category == 3).toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SingleChildScrollView(
            child: Column(children: <Widget>[
      SizedBox(
        height: 20,
      ),
      HomeWidget(context).buildSection("Glasses", () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => MoreGlasses()));
      }),
      SizedBox(
        height: 20,
      ),
      Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Container(
          //height: 180,
          // decoration: BoxDecoration(color: Colors.blue),
          child: Stack(
            children: <Widget>[
              Container(
                height: 180,
                //color: Colors.pinkAccent,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: lstGlasses.length,
                    //itemCount: 3,
                    itemBuilder: (_, i) => ItemStore(
                          item: lstGlasses[i],
                        )),
              )
            ],
          ),
        ),
      ),
    ])));
  }
}
