import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:store/DetailScreen/ItemDetail.dart';
import 'package:store/HomeWidget.dart';
import 'package:store/ScreenWidget/glassesPage.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/controller/converjson.dart';
import 'package:store/models/ItemStore.dart';
import 'package:store/viewAll/moreWatches.dart';
//import 'package:store/models/textStyle.dart';

class WatchPage extends StatefulWidget {
  WatchPage({
    Key? key,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

void onTap(BuildContext context) {
  Navigator.of(context).pushNamed(ItemDetail.routeName, arguments: HotDeal());
}

class _HomeScreenState extends State<WatchPage> {
  bool isLoading = true;
  List<HotDeal> lstHotdeal = <HotDeal>[];
  List<HotDeal> lstWatch = <HotDeal>[];
  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await getHotdeal();
  }

  getHotdeal() async {
    await ServiceHotdeal.convertAssets().then((data) {
      setState(() {
        // isLoading = false;
        lstHotdeal = data;
        lstWatch =
            lstHotdeal.where((element) => element.category == 2).toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SingleChildScrollView(
            child: Column(children: <Widget>[
      SizedBox(
        height: 20,
      ),
      HomeWidget(context).buildSection("Watches", () {
     
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => MoreWatches()));
      }),
      SizedBox(
        height: 20,
      ),
      Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Container(
          //height: 180,
          // decoration: BoxDecoration(color: Colors.blue),
          child: Stack(
            children: <Widget>[
              Container(
                height: 180,
                // color: Colors.pinkAccent,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: lstWatch.length,
                    //itemCount: 3,
                    itemBuilder: (_, i) => ItemStore(
                          item: lstWatch[i],
                        )),
              ),
            ],
          ),
        ),
      ),
    ])));
  }
}
