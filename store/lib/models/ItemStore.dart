import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:store/DetailScreen/ItemDetail.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/models/textStyle.dart';

class ItemStore extends StatefulWidget {
  final HotDeal? item;
  ItemStore({
    Key? key,
    this.item,
  }) : super(key: key);

  @override
  _HotDealItemState createState() => _HotDealItemState(items: item);
}

class _HotDealItemState extends State<ItemStore> {
  var items;
  _HotDealItemState({this.items});
  HotDeal item = HotDeal();

  // void onTap(BuildContext context) {
  //   Navigator.of(context).pushNamed(ItemDetail.routeName, arguments: item);
  // }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        var a = items.image.toString();
        var n = items.name.toString();
        var p = items.price.toString();
        var d = items.description.toString();
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ItemDetail(
                    a: a,
                    n: n,
                    p: p,
                    d: d,
                  )),
        );
      },
      child: Container(
        width: 125,
        decoration: BoxDecoration(
            //boxShadow: BoxShadow.(offset: 2),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            border: Border.all(
              color: Colors.grey.shade200,
              width: 1.5,
            )),
        margin: EdgeInsets.only(right: 10),
        child: LayoutBuilder(builder: (_, constraints) {
          return Column(
            children: <Widget>[
              Container(
                // height: 120,
                // decoration:
                //     BoxDecoration(color: Colors.purple),
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        height: 120,
                        decoration: BoxDecoration(
                            //color: Colors.yellow,
                            // border: Border.all(
                            //     color: Color(0xff737988), width: 2.5),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.5),
                      child: Container(
                        child: Image.asset(
                            //'assets/item/pen.png',
                            '${items.image}',
                            height: constraints.maxHeight * 0.5),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 10),
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              '${items.price} ฿',
                              style: txtPri,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              '${items.name}',
                              style: txtDes,
                            ),
                          ],
                        ),
                      ],
                    )),
              ),
            ],
          );
        }),
      ),
    );
  }
}

