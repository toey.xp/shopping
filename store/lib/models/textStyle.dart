import 'package:flutter/material.dart';

const txtTitle =
    TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black);
const txtView =
    TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.red);
const txtPri = TextStyle(
    fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff7d7d));
const txtDes = TextStyle(
    fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff737988));