class HotDeal {
  String? image;
  String? name;
  String? price;
  int? category;
  String? description;

  HotDeal({this.image, this.name, this.price, this.category, this.description});

  factory HotDeal.fromJson(Map<String, dynamic> json) {
    return HotDeal(
        image: json["image"],
        name: json["name"],
        price: json["price"],
        category: json["category"],
        description: json["description"]);
  }
}
