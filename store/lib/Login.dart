import 'package:flutter/material.dart';
import 'package:store/HomeScreen.dart';
import 'package:store/User.dart';
import 'package:store/controller/userconvert.dart';
import 'package:store/main.dart';
import 'package:store/models/textStyle.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController userNameController = new TextEditingController();
  TextEditingController passWordController = new TextEditingController();
  List<User> lstUser = <User>[];
  bool loading = true;

  @override
  void didChangeDependencies() async {
    //ใช้เพื่อให้รอกันจากบนลงล่าง ไม่งั้นจะทำพร้อมกัน
    super.didChangeDependencies();
    await getUser();
  }

  getUser() {
    ServiceUser.convertAssets().then((data) {
      //เขียวคลาส เหลืองฟังกชัน
      setState(() {
        //ทำฟังชั้นนี่่ก่อน
        loading = false; //ไม่ต้องโหลดแล้ว
        lstUser = data;
      });
    });
  }

  chkUser(String username, String password) {
    //เปรียบเทียบค่าที่ได้มาว่าตรงกับในระบบหรือไม่
    var haveUser = lstUser
        .where((x) => x.username == username && x.password == password)
        .isNotEmpty;
    if (haveUser) {
      setState(() {
        isLogin = true;
        //ถ้ามี user ให้ทำที่คลาส HomePage
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomeScreen()));
      });
    } else {
      // ถ้าไม่มีให้ขึ้นข้อความ
      final snackBar = SnackBar(
        content: const Text('Please check your username and password'),
        action: SnackBarAction(label: 'OK', onPressed: () {}),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 100.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Shopping',
                      style: TextStyle(
                          fontSize: 60,
                          color: Colors.blueGrey[800],
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                child: Container(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(color: Colors.grey.shade300, blurRadius: 10.0)
                  ], borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    elevation: 2,
                    child: Column(
                      children: [
                        SizedBox(height: 30),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          height: 55,
                          //width: 315,
                          width: MediaQuery.of(context).size.width / 1.3,
                          decoration: BoxDecoration(
                            color: Color(0xffe2e2e2),
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 5.0,
                              left: 5.0,
                              right: 10.0,
                            ),
                            child: TextField(
                              controller: userNameController,
                              style: TextStyle(color: Colors.black),
                              textAlign: TextAlign.start,
                              decoration: InputDecoration(
                                icon: Icon(
                                  Icons.person,
                                  color: Colors.grey.shade600,
                                ),

                                // fillColor: Color(0xffe2e2e2),
                                // filled: true,
                                border: OutlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.transparent),
                                  borderRadius: BorderRadius.circular(50.0),
                                ),
                                hintText: "Username Or Email",
                                hintStyle:
                                    TextStyle(color: Colors.grey.shade700),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.transparent),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.transparent),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50))),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          height: 55,
                          //width: 315,
                          width: MediaQuery.of(context).size.width / 1.3,
                          decoration: BoxDecoration(
                            color: Color(0xffe2e2e2),
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 5.0,
                              left: 10.0,
                              right: 10.0,
                            ),
                            child: TextField(
                              controller:
                                  passWordController, //รูปแบบการเก็บตัวแปรของ textfield เก็บตัวแปรเพื่อเอาไปใช้
                              style: TextStyle(color: Colors.black),
                              textAlign: TextAlign.start,
                              decoration: InputDecoration(
                                // fillColor: Color(0xffe2e2e2),
                                // filled: true,
                                icon: Icon(
                                  Icons.lock,
                                  color: Colors.grey.shade600,
                                ),
                                hintText: "Password",

                                enabledBorder: OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.transparent),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.transparent),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50))),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 20,
                            right: 20,
                          ),
                          child: Container(
                            height: 55,
                            width: MediaQuery.of(context).size.width,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      Colors.blueGrey[800]),
                                  shape: MaterialStateProperty.all(
                                      const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20.0))))),
                              onPressed: () {
                                var username = userNameController.text;
                                var password = passWordController.text;

                                chkUser(username, password);
                              },
                              child: Text(
                                'SIGN IN',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 30),
                      ],
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Already have an account?  "),
                  Text("Sign in!!",
                      style: TextStyle(
                        color: Colors.blue,
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
