import 'package:flutter/material.dart';
import 'package:store/Login.dart';
import 'package:store/controller/converjson.dart';
import 'package:store/main.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/models/textStyle.dart';



class ItemDetail extends StatefulWidget {
  final HotDeal? item;
  final String? a;
  final String? n;
  final String? p;
  final String? d;
  ItemDetail({
    Key? key,
    this.item,
    this.a,
    this.n,
    this.p,
    this.d,
  }) : super(key: key);

  static const routeName = 'ItemDetail/';

  @override
  _ItemDetailState createState() =>
      _ItemDetailState(items: item, a: a, n: n, p: p, d: d);
}

class _ItemDetailState extends State<ItemDetail> {
  HotDeal? items;
  String? a;
  String? n;
  String? p;
  String? d;
  bool _initLoaded = true;
  _ItemDetailState({this.items, this.a, this.n, this.p, this.d});
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(
      //title: Text("data"),
      // ),
      body: Container(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                AspectRatio(
                  aspectRatio: 1.2,
                  child: Container(
                    padding:
                        const EdgeInsets.only(left: 15, top: 30, right: 15),
                    decoration: BoxDecoration(color: Colors.blueGrey[50]),
                    child: LayoutBuilder(
                      builder:
                          (BuildContext context, BoxConstraints constraints) {
                        return Column(
                          children: [
                            InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              child: Row(
                                children: [Icon(Icons.arrow_back_ios_new)],
                              ),
                            ),
                            Image.asset(
                              //"assets/item/cats.png",

                              // '${items.image}',
                              // height: constraints.maxHeight * 0.5,

                              '$a',

                              height: MediaQuery.of(context).size.height * 0.3,
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
                // SizedBox(
                //   height: 200,
                // ),
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      '$n',
                      style: txtTitle,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      '$p  ฿',
                      style: txtPri,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      '$d ',
                      style: txtDes,
                    ),
                  ),
                ),

                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: ItemCounter(),
    );
  }
}

class ItemCounter extends StatefulWidget {
  ItemCounter({
    Key? key,
  }) : super(key: key);

  @override
  _ItemCounterState createState() => _ItemCounterState();
}

class _ItemCounterState extends State<ItemCounter> {
  alertMsg(String msg) {
    // set up the buttons

    Widget continueButton = TextButton(
      child: Text(
        "ตกลง",
        style: TextStyle(fontFamily: 'Kanit'),
      ),
      onPressed: () async {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(
        msg,
        style: TextStyle(fontFamily: 'Kanit'),
      ),
      actions: [
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  int num = 1;
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        shape: CircularNotchedRectangle(),
        //notchMargin: 5.0,
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 110,
            decoration: BoxDecoration(
                color: Colors.blueGrey[900],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0))),
            child: Column(children: [
              SizedBox(
                height: 20,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Quantity",
                        style: txtView,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 30.0),
                      child: Container(
                        height: 45,
                        width: MediaQuery.of(context).size.width / 2.5,
                        decoration: BoxDecoration(
                            //color: Colors.white,
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            )),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 3,
                              child: Container(
                                width: MediaQuery.of(context).size.width / 2.5 -
                                    100,
                                child: Text(
                                  //"1",
                                  num.toString().padLeft(2),
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Container(
                              width: 100,
                              child: Row(
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        if (num > 1)
                                          setState(() {
                                            num--;
                                          });
                                      },
                                      icon: Icon(
                                        Icons.remove_sharp,
                                        color: Colors.white,
                                        size: 20,
                                      )),
                                  IconButton(
                                      onPressed: () {
                                        setState(() {
                                          num++;
                                        });
                                      },
                                      icon: Icon(
                                        Icons.add,
                                        color: Colors.white,
                                        size: 20,
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 30.0),
                      child: InkWell(
                        onTap: () {
                          if (isLogin == false) {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginPage()));
                          } else {
                            if (num >= 10) {
                              alertMsg('สินค้าหมด');
                            } else if (num >= 1 && num <= 8) {
                              alertMsg('เพิ่มสินค้าแล้ว');
                            } else if (num == 9) {
                              alertMsg('สินค้าอยุ่ในตะกร้าแล้ว');
                            }
                          }
                        },
                        child: Container(
                            alignment: Alignment.center,
                            height: 45,
                            width: MediaQuery.of(context).size.width / 2.5,
                            decoration: BoxDecoration(
                                //color: Colors.white,
                                border: Border.all(color: Colors.white),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                )),
                            child: Text(
                              "Add to Cart",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                              textAlign: TextAlign.center,
                            )),
                      ),
                    ),
                  ]))
            ])));
  }
}
              
