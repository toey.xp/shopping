import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:store/ScreenWidget/glassesPage.dart';

import 'package:store/models/textStyle.dart';
import 'package:store/viewAll/models/moreGlasses.dart';
import 'package:store/viewAll/models/moreShoes.dart';
import 'package:store/viewAll/models/moreWatches.dart';
import 'package:store/viewAll/models/moreItemCard.dart';
import 'package:store/viewAll/moreWatches.dart';

class HomeWidget {
  final BuildContext context;
  HomeWidget(this.context);

  Widget buildSection(String title, Function onTap) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: txtTitle,
          ),
          InkWell(
            onTap: () => onTap(),
            child: Text(
              "View all",
              style: txtView,
            ),
          ),
        ],
      ),
    );
  }
}
