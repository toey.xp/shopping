//import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:store/ScreenWidget/CartPage.dart';
import 'package:store/ScreenWidget/glassesPage.dart';

import 'package:store/ScreenWidget/watchPage.dart';

import 'package:store/ScreenWidget/shoePage.dart';
import 'package:store/main.dart';

//import 'package:store/controller/converjson.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // bool isLoading = true;
  int? _selectIndex = 0;

  void _onItemTapped(int? index) {
    setState(() {
      _selectIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    print(isLogin);
    List<Widget> _tapbar = <Widget>[
      //isLoading
      // ? Center(
      //     child: CircularProgressIndicator(
      //     valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
      //   ))
      // :
      Column(
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Stack(
                children: [
                  Container(
                    height: 255,
                    width: MediaQuery.of(context).size.width,
                  ),
                  Container(
                    height: 190,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(color: Colors.blueGrey[800]),
                  ),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(height: 140),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          height: 50,
                          //width: 315,
                          width: MediaQuery.of(context).size.width / 1.32,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextField(
                            decoration: InputDecoration(
                              icon: Icon(
                                Icons.search_sharp,
                                size: 30,
                              ),
                              hintText: "Search",
                              hintStyle: TextStyle(
                                color: Colors.black,
                              ),
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CartPage()));
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                color: Colors.orange, shape: BoxShape.circle),
                            child: Icon(
                              Icons.shopping_cart_outlined,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                      bottom: 0,
                      left: 15,
                      right: 15,
                      top: 120,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: Image(
                          //height: 20
                          //0,
                          // width: 350,
                          fit: BoxFit.cover,
                          image: AssetImage('assets/item/banner.jpeg'),
                        ),
                      ))
                ],
              ),
            ),
          ),
          WatchPage(),
          ShoesPage(),
          GlassesPage(),
        ],
      ),

      Padding(
        padding: const EdgeInsets.all(50.0),
        child: Column(
          children: [Text("Alert")],
        ),
      ),
      Column(
        children: [Text("Favorite")],
      ),
      Column(
        children: [Text("Cart")],
      ),
      Column(
        children: [Text("Account")],
      ),
    ];
    return Container(
      child: Scaffold(
        body: SingleChildScrollView(
          child: _tapbar.elementAt(_selectIndex!),
          // child: Column(
          //   children: [
          //     HomeWidget(context).homestore(),
          //     HotdealPage(),
          //     TrendyPage(),
          //   ],
          // ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white70,

          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home_outlined), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.notifications_outlined), label: 'Alert'),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite_border_outlined), label: 'Favorite'),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart_outlined), label: 'Cart'),
            BottomNavigationBarItem(
                icon: Icon(Icons.person_outlined), label: 'Account'),
          ],
          unselectedItemColor:
              Colors.grey[700], //ตรงไอคอนเมื่อยังไม่ได้เลือกเป็นสีเทา
          selectedItemColor: Colors.red[700],
          currentIndex: _selectIndex!,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
