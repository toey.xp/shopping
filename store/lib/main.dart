import 'package:flutter/material.dart';
import 'package:store/DetailScreen/ItemDetail.dart';
//import 'package:store/DetailScreen/ItemDetail.dart';
import 'package:store/HomeScreen.dart';
import 'package:store/Login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
      routes: routes,
    );
  }
}

final Map<String, Widget Function(BuildContext)> routes =
//const <String, WidgetBuilder>
    {ItemDetail.routeName: (_) => ItemDetail()};

bool isLogin = false;
