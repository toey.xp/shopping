import 'dart:convert';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:flutter/services.dart';

class ServiceHotdeal {
  static Future<List<HotDeal>> convertAssets() async {
    final String response =
        await rootBundle.loadString('assets/json/Hotdeal.json');
    List<HotDeal> lst = parseJsonHotdeal(response);

    if (lst.isNotEmpty) {
      return lst;
    } else {
      return <HotDeal>[];
    }
  }

  

  static List<HotDeal> parseJsonHotdeal(String responseBody) {
    final parse = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parse.map<HotDeal>((json) => HotDeal.fromJson(json)).toList();
  }
}
