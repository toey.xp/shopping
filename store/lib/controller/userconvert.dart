import 'dart:convert';
import 'package:flutter/animation.dart';
import 'package:flutter/foundation.dart';
import 'package:store/User.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:flutter/services.dart';

class ServiceUser {
  static Future<List<User>> convertAssets() async {
    final String response =
        await rootBundle.loadString('assets/json/user.json');
    List<User> lst = parseJsonUser(response);

    if (lst.isNotEmpty) {
      return lst;
    } else {
      return <User>[];
    }
  }

  static List<User> parseJsonUser(String responseBody) {
    final parse = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parse.map<User>((json) => User.fromJson(json)).toList();
  }
}
