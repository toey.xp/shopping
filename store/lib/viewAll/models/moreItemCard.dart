import 'package:flutter/material.dart';
import 'package:store/DetailScreen/ItemDetail.dart';
import 'package:store/controller/converjson.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/models/textStyle.dart';

class MoreItemCard extends StatefulWidget {
  final HotDeal item;
  const MoreItemCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  _MoreItemCardState createState() => _MoreItemCardState();
}

class _MoreItemCardState extends State<MoreItemCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ItemDetail(
                      a: widget.item.image,
                      n: widget.item.name,
                      p: widget.item.price,
                      d: widget.item.description,
                    )));
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: Card(
          elevation: 2,

          // color: Color(0xffe2e2e2),
          shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.grey.shade200, width: 1.5),
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 5,
              ),
              Container(
                height: 110,
                child:
                    //Image.asset("assets/item/x.jpeg"),
                    Image.asset('${widget.item.image}'),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                padding: EdgeInsets.only(left: 25),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  '${widget.item.name}',
                  style: txtTitle,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 25),
                width: MediaQuery.of(context).size.width,
                child: Text(
                  '${widget.item.price}  ฿',
                  style: txtPri,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
