import 'package:flutter/material.dart';
import 'package:store/controller/converjson.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/models/textStyle.dart';
import 'package:store/viewAll/models/moreItemCard.dart';

class MoreShoes extends StatefulWidget {
  final HotDeal? item;
  MoreShoes({
    Key? key,
    this.item,
  }) : super(key: key);

  @override
  _MoreShoesState createState() => _MoreShoesState();
}

class _MoreShoesState extends State<MoreShoes> {
  List<HotDeal> lstShoes = <HotDeal>[];

  List<HotDeal> lstHotdeal = <HotDeal>[];
  //------------------------------------------------------------
  //HotDeal item = HotDeal();

  void didChangeDependencies() async {
    super.didChangeDependencies();
    await getHotdeal();
  }

  getHotdeal() async {
    await ServiceHotdeal.convertAssets().then((data) {
      setState(() {
        //loading = false;
        lstHotdeal = data;
        lstShoes =
            lstHotdeal.where((element) => element.category == 1).toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Shoes",
                style: TextStyle(
                  fontSize: 27,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),
            backgroundColor: Colors.white,
            leading: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ))),
        body: Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  // Container(
                  //   padding: EdgeInsets.only(top: 40, left: 15),
                  //   child: InkWell(
                  //     onTap: () => Navigator.of(context).pop(),
                  //     child: Row(
                  //       children: [
                  //         Icon(Icons.arrow_back_ios_new),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: 5,
                  // ),
                  // Container(
                  //   child: Text(
                  //     "Watches",
                  //     style: TextStyle(
                  //         color: Color(0xff2e2e2e),
                  //         fontSize: 30,
                  //         fontWeight: FontWeight.bold),
                  //   ),
                  // )
                ],
              ),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.only(left: 15.0, right: 15.0),

                    //----------------------------------------------------------------
                    child: GridView.builder(
                      scrollDirection: Axis.vertical,
                      addAutomaticKeepAlives: false,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                      ),
                      itemCount: lstShoes.length,
                      itemBuilder: (BuildContext context, int index) {
                        return MoreItemCard(item: lstShoes[index]);
                      },
                    )),
              )
            ],
          ),
        ));
  }
}
