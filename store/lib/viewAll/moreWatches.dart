import 'package:flutter/material.dart';
import 'package:store/controller/converjson.dart';
import 'package:store/models/Hotdeal.dart';
import 'package:store/models/textStyle.dart';
import 'package:store/viewAll/models/moreItemCard.dart';

class MoreWatches extends StatefulWidget {
  final HotDeal? item;
  MoreWatches({
    Key? key,
    this.item,
  }) : super(key: key);

  @override
  _MoreWatchesState createState() => _MoreWatchesState();
}

class _MoreWatchesState extends State<MoreWatches> {
  List<HotDeal> lstWatch = <HotDeal>[];

  List<HotDeal> lstHotdeal = <HotDeal>[];
  //------------------------------------------------------------
  //HotDeal item = HotDeal();

  void didChangeDependencies() async {
    super.didChangeDependencies();
    await getHotdeal();
  }

  getHotdeal() async {
    await ServiceHotdeal.convertAssets().then((data) {
      setState(() {
        //loading = false;
        lstHotdeal = data;
        lstWatch =
            lstHotdeal.where((element) => element.category == 2).toList();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Watches",
                style: TextStyle(
                  fontSize: 27,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),
            backgroundColor: Colors.white,
            leading: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ))),
        body: Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  // Container(
                  //   padding: EdgeInsets.only(top: 40, left: 15),
                  //   child: InkWell(
                  //     onTap: () => Navigator.of(context).pop(),
                  //     child: Row(
                  //       children: [
                  //         Icon(Icons.arrow_back_ios_new),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: 5,
                  // ),
                  // Container(
                  //   child: Text(
                  //     "Watches",
                  //     style: TextStyle(
                  //         color: Color(0xff2e2e2e),
                  //         fontSize: 30,
                  //         fontWeight: FontWeight.bold),
                  //   ),
                  // )
                ],
              ),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.only(left: 15.0, right: 15.0),

                    //----------------------------------------------------------------
                    child: GridView.builder(
                      scrollDirection: Axis.vertical,
                      addAutomaticKeepAlives: false,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                      ),
                      itemCount: lstWatch.length,
                      itemBuilder: (BuildContext context, int index) {
                        return MoreItemCard(item: lstWatch[index]);
                      },
                    )),
              )
            ],
          ),
        ));
  }
}

// class MoreItemCard extends StatelessWidget {
//   final HotDeal item;
//   const MoreItemCard({
//     Key? key,
//     required this.item,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
//       child: Card(
//         elevation: 2,

//         // color: Color(0xffe2e2e2),
//         shape: RoundedRectangleBorder(
//             side: BorderSide(color: Colors.grey.shade200, width: 1.5),
//             borderRadius: BorderRadius.all(
//               Radius.circular(20),
//             )),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           children: [
//             SizedBox(
//               height: 5,
//             ),
//             Container(
//               height: 110,
//               child:
//                   //Image.asset("assets/item/x.jpeg"),
//                   Image.asset('${item.image}'),
//             ),
//             SizedBox(
//               height: 5,
//             ),
//             Container(
//               padding: EdgeInsets.only(left: 25),
//               width: MediaQuery.of(context).size.width,
//               child: Text(
//                 '${item.name}',
//                 style: txtTitle,
//               ),
//             ),
//             Container(
//               padding: EdgeInsets.only(left: 25),
//               width: MediaQuery.of(context).size.width,
//               child: Text(
//                 '${item.price}  ฿',
//                 style: txtPri,
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
