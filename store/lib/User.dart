class User {
  User({
    this.id,
    required this.username,
    required this.password,
    this.age,
    this.province,
  });
  final int? id;
  final String username;
  final String password;
  final int? age;
  final String? province;

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json["id"],
        username: json["username"],
        password: json["password"],
        age: json["age"],
        province: json["province"]);
  }
}
